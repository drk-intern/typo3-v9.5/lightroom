<?php
namespace KayStrobach\Lightroom\Browser;

use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class FileBrowser extends \TYPO3\CMS\Recordlist\Browser\FileBrowser
{
    /**
     * Defines the rendermode of the selection wizard
     * @var string
     */
    protected $renderMode = null;

    /**
     * @return void
     */
    protected function initialize()
    {
        parent::initialize();
    }

    /**
     * @return void
     */
    protected function initVariables()
    {
        parent::initVariables();
        $this->renderMode = GeneralUtility::_GP('renderMode') ? GeneralUtility::_GP('renderMode') : null;
        if ($this->renderMode === null) {
            $this->renderMode = $GLOBALS['BE_USER']->uc['wizard_element_browser_renderMode'] ? $GLOBALS['BE_USER']->uc['wizard_element_browser_renderMode'] : 'lightroom';
        }
        $GLOBALS['BE_USER']->uc['wizard_element_browser_renderMode'] = $this->renderMode;
        $GLOBALS['BE_USER']->overrideUC();
        $GLOBALS['BE_USER']->writeUC();
    }

    /**
     * For TYPO3 Element Browser: Expand folder of files.
     *
     * @param Folder $folder The folder path to expand
     * @param array $extensionList List of fileextensions to show
     * @param bool $noThumbs Whether to show thumbnails or not. If set, no thumbnails are shown.
     * @return string HTML output
     */
    public function renderFilesInFolder(Folder $folder, array $extensionList = [], $noThumbs = false)
    {
        if ($this->renderMode === 'lightroom') {
            return $this->renderModeSwitch() . $this->renderLightroom($folder, $extensionList, $noThumbs);
        } else {
            return $this->renderModeSwitch() . parent::renderFilesInFolder($folder, $extensionList, $noThumbs);
        }
    }

    public function renderModeSwitch()
    {
        $newMode = ($this->renderMode === 'lightroom') ? 'filelist' : 'lightroom';
        $iconName = ($this->renderMode === 'lightroom') ? 'actions-edit-hide' : 'actions-edit-unhide';
        $urlParams = $_GET;
        ArrayUtility::mergeRecursiveWithOverrule(
            $urlParams,
            [
                'renderMode' => $newMode
            ]
        );
        $uri = GeneralUtility::makeInstance(UriBuilder::class)->buildUriFromRoute('wizard_element_browser', $urlParams);
        return '<div class="module-docheader" style="height: 20px; min-height: 20px;">'
            . '<a href="' . htmlspecialchars($uri) . '" style="text-decoration: none;">'
                    . $this->iconFactory->getIcon($iconName, Icon::SIZE_SMALL)
                    . '&nbsp;'
                    . $this->getLanguageService()->sL('LLL:EXT:lightroom/Resources/Private/Language/locallang.xlf:lightroom')
                .'</a>'
            .'</div>';
    }

    /**
     * @param Folder $folder
     * @param array $extensionList
     * @param bool $noThumbs
     * @return string
     */
    public function renderLightroom(Folder $folder, array $extensionList = [], $noThumbs = false)
    {
        $this->aggregateElements($folder);
        /** @var StandaloneView $view */
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->assign('folder', $folder);
        $view->setTemplatePathAndFilename(
            ExtensionManagementUtility::extPath('lightroom') . 'Resources/Private/Browser/FileBrowser/Lightroom.html'
        );
        return $view->render();
    }

    public function aggregateElements(Folder $folder)
    {
        /** @var File $fileObject */
        foreach ($folder->getFiles() as $fileObject) {
            $filesIndex = count($this->elements);
            $icon = '<span title="' . htmlspecialchars($fileObject->getName()) . '">' . $this->iconFactory->getIconForResource($fileObject, Icon::SIZE_SMALL) . '</span>';

            $this->elements['file_' . $filesIndex] = [
                'type' => 'file',
                'table' => 'sys_file',
                'uid' => $fileObject->getUid(),
                'fileName' => $fileObject->getName(),
                'filePath' => $fileObject->getUid(),
                'fileExt' => $fileObject->getExtension(),
                'fileIcon' => $icon
            ];
        }
    }
}

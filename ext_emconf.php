<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: 'contentmigration'
 *
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * 'version' and 'dependencies' must not be touched!
 ***************************************************************/

$EM_CONF['lightroom'] = array(
    'title' => 'Lightroom',
    'description' => 'Integrates an easy to use lightroom wizard',
    'category' => 'templates',
    'author' => '4viewture GmbH - Kay Strobach',
    'author_email' => 'drk-service@support.4viewture.de',
    'state' => 'stable',
    'clearCacheOnLoad' => true,
    'version' => '11.0.12',
    'constraints' => array(
        'depends' => array(
            'typo3' => '10.4.0-10.4.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
    'autoload' => array(
        'psr-4' => array(
            'KayStrobach\\Lightroom\\' => 'Classes'
        )
    )
);

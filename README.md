Installation
=====

This extension hooks into the add media wizard and allows to add media from a __lightroom__ view.

To access the lightroom view you can easily use the __add media__ buttons a TCA form.

![Standard Media field in TCAForms](Documentation/Images/mediafield.png)

![Wizard containing switch](Documentation/Images/wizard.png)

![Lightroom view](Documentation/Images/lichttisch.png)

